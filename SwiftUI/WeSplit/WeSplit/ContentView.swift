//
//  ContentView.swift
//  WeSplit
//
//  Created by MrBond on 11/2/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var checkAmount = ""
    @State private var numberOfPeople = "2"
    @State private var tipPercentage = 0
    var tipPercentageValues = [0,10,20,25]
    var pesonCount: Double {
        let numberOfPeopleCount = Double(numberOfPeople) ?? 0
        return numberOfPeopleCount
    }
    var totalAmount: Double {
        let amount = Double(checkAmount) ?? 0
        let tipValue = Double(tipPercentageValues[tipPercentage])
        let tipAmount = Double (amount / 100 * tipValue)
        let totalAmount = amount + Double(tipAmount)
        return totalAmount
    }
    var amountForEach: Double {
//        let amount = Double(checkAmount) ?? 0
//        let personCount = Double(numberOfPeople + 2)
//        let tipValue = Double(tipPercentageValues[tipPercentage])
//        let tipPercengate = amount / 100 * tipValue
//        let totalAmount = amount + Double(tipPercentage)
        let eachPersonAmount = totalAmount / pesonCount
        return eachPersonAmount
    }
    var body: some View {
        NavigationView {
            Form {
                      
                  Section {
                      
                      TextField("Enter Billing Amount", text: $checkAmount)
                          .keyboardType(.decimalPad)
//                      Picker("Number of People", selection: $numberOfPeople) {
//                          ForEach(2..<10) { personCount in
//                              Text("\(personCount)People")
//                          }
//                      }
                    TextField("Enter numbers of person", text: $numberOfPeople)
                    .keyboardType(.decimalPad)
                  }
                Section(header: Text("how much tip do you want to give ?")) {
                    Picker("Tip Percentage", selection: $tipPercentage) {
                        ForEach(0 ..< tipPercentageValues.count) { value in
                            Text("\(value)%")
                        }
                    }
                .pickerStyle(SegmentedPickerStyle())
                }
                Section(header: Text("Amount per person")) {
                    Text("$\(amountForEach, specifier: "%.2f")")
                }
                Section(header: Text("Total amount including Tip and Bill ")) {
                    Text("$\(totalAmount, specifier: "%.2f")")
                        .foregroundColor(tipPercentage > 0 ? .black : .red)
                }
            }
        .navigationBarTitle("WeSplit")
        }
       
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
