//
//  ContentView.swift
//  GessTheFlag
//
//  Created by MrBond on 11/4/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import SwiftUI

struct FlagImage: ViewModifier {
    func body(content: Content) -> some View {
        content
            .clipShape(Capsule())
            .overlay(Capsule().stroke(Color.black, lineWidth: 1))
            .shadow(radius: 1)
    }
}
extension View {
    func flagImage() -> some View {
        self.modifier(FlagImage())
    }
}
struct ContentView: View {
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var selectedAnswer: Int!
    @State private var resultAlert = false
    @State private var winString = ""
    @State private var score = 0
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.black,.blue]), startPoint: .top, endPoint: .bottom)
                .edgesIgnoringSafeArea(.all)
            VStack(spacing: 30) {
                VStack(spacing: 10) {
                    Text("Select Flag")
                        .foregroundColor(Color.white)
                      Text(countries[correctAnswer])
                        .font(.system(size: 20))
                        .fontWeight(.black)
                        .foregroundColor(Color.gray)
                    
                }
                    ForEach(0 ..< 3) { number in
                        Button (action: {
                            self.selectedAnswer = number
                            self.checkWinner(winningNumber: number)
                        }) {
                            Image(self.countries[number])
                                .renderingMode(.original)
                                
                        }
                        .flagImage()
                    }
                Text("Score is \(score)")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
        }
        .alert(isPresented: $resultAlert) { () -> Alert in
            Alert(title: Text(winString), message: winString == "WRONG" ? Text("this is the flag of \(self.countries[self.selectedAnswer]) !") : Text("WellDone !!"), dismissButton: .default(Text("Ok")) {
                self.nextQuestion()
                })
        }
    }
    func checkWinner(winningNumber: Int) {
        print(winningNumber, correctAnswer)
        if winningNumber == correctAnswer {
         winString = "CORRECT"
         score += 1
         resultAlert = true
         return
        }
        winString = "WRONG"
        resultAlert = true
    }
    func nextQuestion() {
        print(countries)
        countries = countries.shuffled()
        correctAnswer = Int.random(in: 0...2)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
