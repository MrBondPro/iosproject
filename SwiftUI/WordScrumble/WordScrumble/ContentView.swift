//
//  ContentView.swift
//  WordScrumble
//
//  Created by MrBond on 11/20/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import SwiftUI
struct ContentView: View {
    @State private var wordList = [String]()
    @State private var rootWord = ""
    @State private var insertWord = ""
    
    @State private var alertMsgTitle = ""
    @State private var alertMsgBody = ""
    @State private var alert = false
    @State private var score = 0
    @State private var wordArrayList = [String]()
    var body: some View {
        NavigationView {
                VStack {
                    TextField("Construct a Word from \(rootWord)", text: $insertWord, onCommit: addWord)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
                    .autocapitalization(.none)
                    .disableAutocorrection(false)
                    List(wordList, id: \.self) {
                        Image(systemName: "\($0.count).circle")
                        Text($0)
                    }
                    Text("Score : \(score)")
                        .font(.system(.headline))
                        .fontWeight(.black)
                }
                .navigationBarTitle(Text(rootWord), displayMode: .automatic)
                .navigationBarItems(leading: Button("Reset") {
                    self.wordList = []
                    self.rootWord = self.wordArrayList.randomElement() ?? "Steve"
                    self.score = 0
                    
                })
            .onAppear(perform: startGame)
            .alert(isPresented: $alert) {
                Alert(title: Text(self.alertMsgTitle), message: Text(self.alertMsgBody), dismissButton: .default(Text("OK")))
            }
        }
    }
    //MARK: - Validation
    func isOriginal(word: String) -> Bool {
        !wordList.contains(word)
    }
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        for char in word {
            if let charIndex = tempWord.firstIndex(of: char) {
                tempWord.remove(at: charIndex)
            } else { return false }
        }
        return true
    }
    func isReal(word: String) -> Bool {
        let textChecker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let missSpelledRange = textChecker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        return missSpelledRange.location == NSNotFound
    }
    func isActual(word: String) -> Bool {
        word.count > 3 && word != rootWord
    }
    func addWord() {
        guard isActual(word: insertWord) else { return }
        
        guard isOriginal(word: insertWord) else {
            alertMsgTitle = "word already Present"
            alertMsgBody = "try entering another word"
            alert = true
            return
        }
        guard isPossible(word: insertWord) else {
            alertMsgTitle = "cant construct word"
            alertMsgBody = "\(insertWord) cant be constructed\nIts not a dictionary word !"
            alert = true
            return
        }
        guard isReal(word: insertWord) else {
            alertMsgTitle = "not a dictionary word"
            alertMsgBody = "\(insertWord) is not present in Dictionary"
            alert = true
            return
        }
        wordList.insert(insertWord, at: 0)
        score += 1
        insertWord = ""
        
    }
    func startGame() {
        print("entered !!")
        if let wordArrayPath = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let wordArray = try? String(contentsOf: wordArrayPath) {
                 wordArrayList = wordArray.components(separatedBy: "\n")
                rootWord = wordArrayList.randomElement() ?? "Captain"
            }
        }
        
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
