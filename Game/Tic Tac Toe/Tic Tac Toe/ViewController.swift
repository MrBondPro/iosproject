//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by deba on 6/21/19.
//  Copyright © 2019 DEBA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var player1 = 1
    var blockState = [ 0,0,0,0,0,0,0,0,0]
    var gameState = true, gameDraw = false
    var winningCondition = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBOutlet weak var resultLabel: UILabel!
    

    
    @IBOutlet weak var playLabel: UIButton!
    @IBAction func playButton(_ sender: UIButton) {
        
        for index in 1...9 {
            let button = view.viewWithTag(index) as! UIButton
            DispatchQueue.main.async {
                button.setImage(nil, for: UIControl.State())
            }
            //button.setImage(nil, for: UIControl.State())
        }
        
        player1 = 1
        blockState = Array(repeating: 0, count: 9)
        gameState = true
        gameDraw = false
        resultLabel.isHidden = true
        playLabel.isHidden = true
    
    }
    
    @IBAction func button(_ sender: UIButton) {
        if blockState[sender.tag - 1] == 0 && gameState {
            if player1 == 1 {
                print("cross")
                sender.setImage(UIImage(named: "Cross.png"), for: UIControl.State())
                blockState[sender.tag - 1] = 1
                player1 = 2
            } else if player1 == 2 {
                print("nought")
                sender.setImage(UIImage(named: "Nought.png"), for: UIControl.State())
                blockState[sender.tag - 1] = 2
                player1 = 1
            }
        }
        for states in winningCondition {
            if blockState[states[0]] != 0 && blockState[states[0]] == blockState[states[1]] && blockState[states[1]] == blockState[states[2]] {
                gameState = false
                if blockState[states[0]] == 1 {
                    resultLabel.text = "CROSS HAS OWN"
                } else {
                    resultLabel.text = "NOUGHT HAS OWN"
                }
                resultLabel.isHidden = false
                playLabel.isHidden = false
            }
         }
        var flag = false
        for blockValue in blockState {
            if blockValue == 0 {
                flag = true
                break
            }
        }
        if flag == false {
            gameState = false
            resultLabel.text = "MATCH IS A DRAW"
            resultLabel.isHidden = false
            playLabel.isHidden = false
        }
    }
}

