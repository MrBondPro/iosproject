//
//  DetailDeviceTableViewController.swift
//  CoreData Demo Three
//
//  Created by MrBond on 8/16/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit
import CoreData
class DetailDeviceTableViewController: UITableViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var deviceTypeTextField: UITextField!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    var device: Device?
    var context: NSManagedObjectContext?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Device Detail"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        nameTextField.text = device?.deviceName
        deviceTypeTextField.text = device?.deviceType
        context?.refresh(device!, mergeChanges: true)
        if let devicesBelongToSameOwner = device?.value(forKey: "sameDeviceType") as? [Device] {
            for similarDevice in devicesBelongToSameOwner {
               (device?.deviceType == "IOS DEVICE") ? print("IOS DEVICE", terminator: " :") : print("MAC DEVICE", terminator: " :")
                print(similarDevice.deviceName)
            }
        }
        if let owner = device?.owner {
            ownerNameLabel.text = owner.name
        } else { ownerNameLabel.text = "Click to assign" }
        tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let device = self.device, let name = nameTextField.text, let type = deviceTypeTextField.text {
            device.deviceName = name
            device.deviceType = type
        } else if device == nil {
            if let name = nameTextField.text, let type = deviceTypeTextField.text, !name.isEmpty && !type.isEmpty {
                let device = Device(context: self.context!)
                device.deviceName = name
                device.deviceType = type
            }
        }
        do {
            try context?.save()
        } catch {
            print("error in adding newDevice !!")
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if device != nil && indexPath.section == 2 && indexPath.row == 0 {
            let pvc = storyboard?.instantiateViewController(withIdentifier: "PersonViewController") as! PersonViewController
//                pvc.editBarButton.isEnabled = false
//                pvc.addBarButton.isEnabled = false
                pvc.delegateObj = self
                pvc.personSelected = device?.owner
                do {
                    try context?.save()
                } catch {
                    print("error")
                }
                navigationController?.pushViewController(pvc, animated: true)
           
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
extension DetailDeviceTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension DetailDeviceTableViewController: PersonDelegate {
    func addPersonToDevice(PersonToAdd person: Person?) {
        device?.owner = person
        do {
            try  context?.save()
        } catch {
            print("saving error")
        }
    }
}
