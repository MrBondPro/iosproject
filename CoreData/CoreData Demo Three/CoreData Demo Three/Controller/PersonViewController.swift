//
//  PersonViewController.swift
//  CoreData Demo Three
//
//  Created by MrBond on 8/16/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit
import CoreData
protocol PersonDelegate: AnyObject {
    func addPersonToDevice(PersonToAdd person: Person?)
}


class PersonViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    var persons: [Person]!
    var personSelected: Person?
    var textFieldDidChangeObserver: NSObjectProtocol!
    lazy var context: NSManagedObjectContext = {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        return (delegate?.persistentContainer.viewContext)!
    }()
    weak var delegateObj: PersonDelegate!
    
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Available Persons"
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onClickAddPerson(_:))),editButtonItem]
        //navigationController?.title = "Available Persons"
        loadPersons()
    }
    override func viewWillAppear(_ animated: Bool) {
        loadPersons()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        loadPersons()
//        tblView.reloadData()
//    }
    func loadPersons() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Person")
        do {
            if let personArray = try context.fetch(fetchRequest) as? [Person] {
                persons = personArray.sorted(by: { (person1, person2) -> Bool in
                    person1.name < person2.name
                })
                for person in persons {
                    print(person.name,terminator: "\n")
                } ; print("\n\n")
            }
        } catch {
            print("device detail fetching error !!")
        }
    }
    //weak var nameTextField: UITextField?
    var okButton: UIAlertAction?
    @IBAction func onClickAddPerson(_ sender: Any) {
        setUpAlertController()
    }
    
    func setUpAlertController() {
        let title = NSLocalizedString("Add Person", comment: "")
        let message = NSLocalizedString("Enter Person Name !", comment: "")
        let actionOkTitle = NSLocalizedString("OK", comment: "")
        let actionCancelTitle = NSLocalizedString("Cancel", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addTextField { (textField) in
           // self.nameTextField = textField
            textField.placeholder = "Enter Name"
            self.textFieldDidChangeObserver = NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using: { (notification) in
                if let textField = notification.object as? UITextField {
                    if let text = textField.text {
                        self.okButton?.isEnabled = text.count > 0
                    }
                }
            })
        }
        let okAction = UIAlertAction(title: actionOkTitle, style: .default) { [unowned self](action) in
            let person = Person(context: self.context)
            //print("no of characters : \(self.nameTextField?.text?.count)")
           // person.name = (self.nameTextField?.text)!
            if let textField = alertController.textFields?.first, let name = textField.text {
                person.name = name
            }
            do {
                try self.context.save()
            } catch {
                print("adding person name error")
                let alertConroller = UIAlertController(title: "naming error", message: "name most have more than one character !!", preferredStyle: .alert)
                alertConroller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertConroller, animated: true, completion: nil)
                self.context.delete(person)
            }
            self.loadPersons()
            self.tblView.reloadData()
        }
        let cancelAction = UIAlertAction(title: actionCancelTitle, style: .cancel) { (action) in
            // do nothing
        }
        okButton = okAction
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        okButton?.isEnabled = false
        present(alertController, animated: true, completion: nil)
    }
    
}

extension PersonViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
        cell.textLabel?.text = "\(persons[indexPath.row].name)"
        if persons[indexPath.row] == personSelected {
            cell.accessoryType = .checkmark
        } else { cell.accessoryType = .none }
        return cell
    }
}
extension PersonViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegateObj == nil {
            let dvc = storyboard?.instantiateViewController(withIdentifier: "DevicesViewController") as! DevicesViewController
                dvc.selectedPerson = persons[indexPath.row]
            navigationController?.pushViewController(dvc, animated: true)
        } else {
            if personSelected == persons[indexPath.row] {
                personSelected = nil
                delegateObj.addPersonToDevice(PersonToAdd: nil)
                //tblView.cellForRow(at: indexPath)?.accessoryType = .none
            } else {
                personSelected = persons[indexPath.row]
                delegateObj.addPersonToDevice(PersonToAdd: persons[indexPath.row])
            }
            
            tableView.reloadData()
//            print("delegate assigned ")
//            if selectedPerson != nil {
//                if let index = selectedPerson?.selectedIndexPath {
//                    tblView.cellForRow(at: index)?.accessoryType = .none
//                    tblView.cellForRow(at: indexPath)?.accessoryType = .checkmark
//                    if let personArray = persons {
//                        selectedPerson = SelectedPerson(personSelected: personArray[index.row], selectedIndexPath: index)
//                        delegateObj.addPersonToDevice(PersonToAdd: personArray[index.row])
//                    }
//
//                } else { print("wronggggg")}
//
//            } else {
//                tblView.cellForRow(at: indexPath)?.accessoryType = .checkmark
//                    if let personArray = persons {
//                        selectedPerson = SelectedPerson(personSelected: personArray[indexPath.row], selectedIndexPath: indexPath)
//                        delegateObj.addPersonToDevice(PersonToAdd: personArray[indexPath.row])
//                    }
//            }
        }
        tblView.deselectRow(at: indexPath, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return delegateObj == nil
//    }
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            let person = persons[indexPath.row]
//            context.delete(person)
//            do {
//                try context.save()
//            } catch {
//                print("data saving error after deletion !!")
//            }
//            loadPersons()
//            tblView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
//           // tblView.reloadData()
//        }
//    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            let person = self.persons[indexPath.row]
            self.persons.remove(at: indexPath.row)
            self.context.delete(person)
            do {
                try self.context.save()
            } catch {
                print("saving error ")
            }
            self.tblView.deleteRows(at: [indexPath], with: .none)
            completionHandler(true)
        }
        let shareAction = UIContextualAction(style: .normal, title: "info") { (action, sourceView, completionHandler) in
            completionHandler(true)
        }
        deleteAction.image = UIImage(named: "delete")
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
        
        return swipeConfiguration
    }
    
}
