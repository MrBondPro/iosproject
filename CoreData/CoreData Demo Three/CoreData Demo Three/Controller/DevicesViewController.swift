//
//  DevicesViewController.swift
//  CoreData Demo Three
//
//  Created by MrBond on 8/16/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit
import CoreData
class DevicesViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    var devices: [Device]!
    var selectedPerson: Person?
    lazy var context: NSManagedObjectContext = {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        return (delegate?.persistentContainer.viewContext)!
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.rowHeight = tblView.estimatedRowHeight
        tblView.estimatedRowHeight = UITableView.automaticDimension
        if selectedPerson == nil {
            navigationItem.title = "Available devices"
        } else {
            navigationItem.title = "\((selectedPerson?.name)!)'s devices"
            onClickAddButton.isEnabled = false
        }
        print("view did lload")

        //navigationController?.title = "Available  Devices"
    }
    
    @IBOutlet weak var onClickAddButton: UIBarButtonItem!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        loadDevices()
        tblView.reloadData()
    }
    func loadDevices(filterPredicateString: String? = nil) {
        if selectedPerson == nil {
            print("executed")
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Device")
            if let predicateString = filterPredicateString {
                let filterPredicate = NSPredicate(format: "deviceType == '\(predicateString)'")
                fetchRequest.predicate = filterPredicate
            }
            do {
                if let deviceArray = try context.fetch(fetchRequest) as? [Device] {
                    devices = deviceArray
                }
            } catch {
                print("device detail fetching error !!")
            }
        } else {
            if let personDevices = selectedPerson?.devices.allObjects as? [Device] {
                devices = personDevices
            }
        }
      
        tblView.reloadData()
        
    }
    
    @IBAction func onClickAdd(_ sender: Any) {
        let dvc = storyboard?.instantiateViewController(withIdentifier: "DetailDeviceTableViewController") as! DetailDeviceTableViewController
        dvc.context = self.context
        navigationController?.pushViewController(dvc, animated: true)
    }
    @IBAction func onClickFilter(_ sender: Any) {
        print("start")
        setUpFilterAlert()
        tblView.reloadData()
    }
    private func setUpFilterAlert() {
        let title = NSLocalizedString("Filter Devices", comment: "")
        let message = NSLocalizedString("select the Type of Device", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "IOS Devices", style: .default, handler: { (action) in
            self.loadDevices(filterPredicateString: "IOS DEVICE")
        }))
        alertController.addAction(UIAlertAction(title: "MAC Devices", style: .default, handler: { (action) in
            print("executed")
            self.loadDevices(filterPredicateString: "MAC DEVICE")
        }))
        alertController.addAction(UIAlertAction(title: "ALL Devices", style: .default, handler: { (action) in
            self.loadDevices()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
            
        }))
        print("end")
        present(alertController, animated: true, completion: nil)
        
    }
}
extension DevicesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("count : \(devices.count)")
        return devices.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! DeviceViewCell
            //        cell.textLabel?.text = "name: \(devices[indexPath.row].deviceName), type: \(devices[indexPath.row].deviceType)"
            cell.cellDevice = devices[indexPath.row]
            return cell
    }
}

extension DevicesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tapped")
        if selectedPerson == nil {
            let dvc = storyboard?.instantiateViewController(withIdentifier: "DetailDeviceTableViewController") as! DetailDeviceTableViewController
            dvc.context = self.context
            dvc.device = devices[indexPath.row]
            navigationController?.pushViewController(dvc, animated: true)
            
        }
       tblView.deselectRow(at: indexPath, animated: true)
    }
}
