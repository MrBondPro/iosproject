//
//  DeviceViewCell.swift
//  CoreData Demo Three
//
//  Created by MrBond on 8/17/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit

class DeviceViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var cellDevice: Device? {
        didSet {
            updateUI()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func updateUI() {
        if let device = cellDevice {
            nameLabel.text = "name: \(device.deviceName) type: \(device.deviceType)"
        }
//        ownerLabel.text = cellDevice?.owner?.name ?? "Owner to be assigned"
        if let ownerName = cellDevice?.owner?.name {
            ownerLabel.text = "OWNER : \(ownerName)"
        } else { ownerLabel.text =  "Owner to be assigned" }
    }
}
