//
//  Device+CoreDataProperties.swift
//  CoreData Demo Three
//
//  Created by MrBond on 8/17/19.
//  Copyright © 2019 Deba. All rights reserved.
//
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device")
    }

    @NSManaged public var deviceName: String
    @NSManaged public var deviceType: String
    @NSManaged public var owner: Person?

}
