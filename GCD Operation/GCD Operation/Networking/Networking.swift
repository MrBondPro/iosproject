//
//  Networking.swift
//  GCD Operation
//
//  Created by Home on 8/8/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import Foundation
import  UIKit
class Networking {
    
    // MARK : - Download image with URLSession and update the image from within the function
    
    class func downloader(url: String, image: UIImageView!) {
        let session = URLSession.shared
        let url = URL(string: url)
        let request = URLRequest(url: url!)
        let downloadTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil && data != nil {
                if let imageData = data {
                    print("data received ")
                    DispatchQueue.main.async {
                        image.image = UIImage(data: imageData)
                    }
                    
                }
            } else {
                print((error?.localizedDescription)!)
            }
        }
        downloadTask.resume()
    }
    class func downloaderWithComplitionHandler(url: String, complitionHanlder: @escaping (UIImage) -> Void) {
        let session = URLSession.shared
        let url = URL(string: url)
        let request = URLRequest(url: url!)
        let downloadTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil && data != nil {
                if let imageData = data {
                    print("data received ")
                    if let imageData = UIImage(data: imageData) {
                        complitionHanlder(imageData)
                    }
                }
            } else {
                print((error?.localizedDescription)!)
            }
        }
        downloadTask.resume()
    }
    
    // MARK : - Download image with URLSession and return the downloaded object / image
    
    class func newDownloader(url: String) -> Data? {
        var tempData: Data?
        let session = URLSession.shared
        let url = URL(string: url)
        let request = URLRequest(url: url!)
        let downloadTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil && data != nil {
                if let imageData = data {
                    print("data received ")
                    tempData = imageData
                }
            } else {
                print((error?.localizedDescription)!)
            }
        }
        downloadTask.resume()
        return tempData
    }
    
    // MARK :- Download with data object  // given in example 
    
    class func imageDownloader(url: String) -> UIImage! {
        let data = try! Data(contentsOf: URL(string: url)!)
        return UIImage(data: data)
    }
}
