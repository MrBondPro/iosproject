//
//  ViewController.swift
//  GCD Operation
//
//  Created by Home on 8/8/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    
    let imageURLs = ["https://s1.ax1x.com/2017/12/06/oakQS.jpg", "https://s1.ax1x.com/2017/12/06/oaiz8.png", "http://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg", "http://adriatic-lines.com/wp-content/uploads/2015/04/canal-of-Venice.jpg"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func onClickDownload(_ sender: Any) {
       // var dataFile: Data?
        
        image1.image = nil
        image2.image = nil
        image3.image = nil
        image4.image = nil
        
        //################################# 1. 1st Logic
        
//        let serialQueue = DispatchQueue(label: "serial Queue")
//        serialQueue.async {
//            dataFile = Networking.newDownloader(url: self.imageURLs[0])
//            if let data = dataFile, let imageFile = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.image1.image = imageFile
//                }
//            } else { print("no data") }
//        }
//        serialQueue.async {
//            dataFile = Networking.newDownloader(url: self.imageURLs[1])
//            if let data = dataFile, let imageFile = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.image2.image = imageFile
//                }
//            } else { print("no data") }
//        }
//        serialQueue.async {
//            dataFile = Networking.newDownloader(url: self.imageURLs[2])
//            if let data = dataFile, let imageFile = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.image3.image = imageFile
//                }
//            } else { print("no data") }
//        }
//        serialQueue.async {
//            dataFile = Networking.newDownloader(url: self.imageURLs[3])
//            if let data = dataFile, let imageFile = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.image4.image = imageFile
//                }
//            } else { print("no data") }
//        }
        
        //#################################     2. 2nd logic
        
        let secondSerialQueue = DispatchQueue(label: "SecondSerialQueue")
//        secondSerialQueue.async {
//            Networking.downloader(url: self.imageURLs[0],image: self.image1)
//        }
        // complition handler code :
        
        secondSerialQueue.async {
            Networking.downloaderWithComplitionHandler(url: self.imageURLs[0]) { image in
                DispatchQueue.main.async { [unowned self] in
                    print("in main closure 1")
                    self.image1.image = image
                }
            }
        }
        secondSerialQueue.async {
            Networking.downloaderWithComplitionHandler(url: self.imageURLs[1]) { image in
                DispatchQueue.main.async { [unowned self] in
                    print("in main closure 2")
                    self.image2.image = image
                }
            }
        }
        secondSerialQueue.async {
            Networking.downloaderWithComplitionHandler(url: self.imageURLs[2]) { image in
                DispatchQueue.main.async { [unowned self] in
                    print("in main closure 3")
                    self.image3.image = image
                }
            }
        }
        secondSerialQueue.async {
            Networking.downloaderWithComplitionHandler(url: self.imageURLs[3]) { image in
                DispatchQueue.main.async { [unowned self] in
                    print("in main closure 4")
                    self.image4.image = image
                }
            }
        }
        // end
//        secondSerialQueue.async {
//            Networking.downloader(url: self.imageURLs[1],image: self.image2)
//        }
//        secondSerialQueue.async {
//            Networking.downloader(url: self.imageURLs[2],image: self.image3)
//        }
//        secondSerialQueue.async {
//            Networking.downloader(url: self.imageURLs[3],image: self.image4)
//        }
        
      //  #################################     3. 3rd logic
        
//        let newSerialQueue = DispatchQueue(label: "newSerialQueue")
//        newSerialQueue.async {
//            let img = Networking.imageDownloader(url: self.imageURLs[0])
//            DispatchQueue.main.async {
//                self.image1.image = img
//            }
//        }
//        newSerialQueue.async {
//            let img = Networking.imageDownloader(url: self.imageURLs[1])
//            DispatchQueue.main.async {
//                self.image2.image = img
//            }
//        }
//        newSerialQueue.async {
//            let img = Networking.imageDownloader(url: self.imageURLs[2])
//            DispatchQueue.main.async {
//                self.image3.image = img
//            }
//        }
//        newSerialQueue.async {
//            let img = Networking.imageDownloader(url: self.imageURLs[3])
//            DispatchQueue.main.async {
//                self.image4.image = img
//            }
//        }
//
        //############
    }
    
}
