//
//  ViewController.swift
//  MapKitDemo
//
//  Created by MrBond on 9/24/19.
//  Copyright © 2019 Deba. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressLabel: UILabel!
    let locationManager = CLLocationManager()
    var previousLocation: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        checkDeviceAuthorization()
    }
    func setLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    func checkAppAuthorization() {
        switch CLLocationManager.authorizationStatus() {
            case .authorizedWhenInUse:
                print("authorizedWhenInUse")
                mapView.showsUserLocation = true
                previousLocation = centerLocation(mapView: mapView)
                centerViewOnUserLocation()
                locationManager.startUpdatingLocation()
            case .notDetermined:
                print("notDetermined")
                locationManager.requestWhenInUseAuthorization()
            case .denied:
                print("denied")
            case .restricted:
                print("restricted")
            case .authorizedAlways:
                print("authorizedAlways")
        }
    }
    func checkDeviceAuthorization() {
        if CLLocationManager.locationServicesEnabled() {
            setLocationManager()
            checkAppAuthorization()
        }
    }
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
            mapView.setRegion(region, animated: true)
        }
    }
    func centerLocation(mapView: MKMapView) -> CLLocation {
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        return CLLocation(latitude: latitude, longitude: longitude)
    }

}
extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let centerLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: centerLocation, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(region, animated: true)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAppAuthorization()
    }
}
extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = centerLocation(mapView: mapView)
        let geocoder = CLGeocoder()
        guard let previousLocation = self.previousLocation, center.distance(from: previousLocation) > 50 else { return }
        self.previousLocation = center
        geocoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            }
            guard let placemark = placemarks?.first else { return }
            let subThroughFare = placemark.subThoroughfare ?? ""
            let throughFare = placemark.thoroughfare ?? ""
            self.addressLabel.text = "\(subThroughFare) \(throughFare)"
        }
        
    }
}
