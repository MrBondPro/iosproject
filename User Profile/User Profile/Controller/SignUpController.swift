//
//  SignUpController.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import UIKit
class SignUpController: UIViewController {
//MARK: - Properties
    @IBOutlet weak var nameField: UITextField!
    let coreDataStack = CoreDataStack()
    //MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.title = "Welcome"
    }
    
//MARK: - Handlers
    @IBAction func onClickContinue(_ sender: Any) {
        if nameField.text == "" {
            let alertController = UIAlertController(title: "Oops", message: "Name can't be empty !", preferredStyle: .alert
            )
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
            return
        }
        let userEntity = User(context: coreDataStack.persistentContainer.viewContext)
        if let name = nameField.text {
            userEntity.name = name
            userEntity.profileImage = nil
            UserDefaults.standard.set("\(name)", forKey: "userName")
            if coreDataStack.persistentContainer.viewContext.hasChanges {
                do {
                    try coreDataStack.persistentContainer.viewContext.save()
                } catch {
                }
            }
        }
        print(nameField.text ?? "default name")
        UserDefaults.standard.set("RootViewCreated", forKey: "rootView")
        let hsc = storyboard?.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        hsc.modalPresentationStyle = .fullScreen
        let nav = UINavigationController(rootViewController: hsc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    

}
