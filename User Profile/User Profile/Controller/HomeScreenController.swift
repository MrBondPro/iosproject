//
//  HomeScreenController.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import CoreData

struct UserProfile {
    var name: String?
    var profilePhoto: UIImage?
}

protocol HomeScreenDelegate: AnyObject {
    func passProfileImage(imageToPass: UIImage?)
}

class HomeScreenController: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    var userData: User!
    var newUserData =  UserProfile()
    var loggedUserName: String!
    var coreDataStack = CoreDataStack()
    var fetchedHomeProfile: UIImage!
    //MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Home Screen"
            loadData()
            setUpTapGesture()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        do {
            try coreDataStack.persistentContainer.viewContext.save()
        } catch {
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        assignUserData()
    }
    //MARK: - Handlers
    
    private func assignUserData() {
        if let profilePhoto = newUserData.profilePhoto {
            profileImageView.image = profilePhoto
        } else { profileImageView.image = UIImage(named: "default_Profile_Photo") }
        profileNameLabel.text = (newUserData.name?.uppercased())!
    }
    
    private func loadData() {
        loggedUserName = UserDefaults.standard.value(forKey: "userName") as? String
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let predicate = NSPredicate(format: "name == '\(loggedUserName!)'")
        fetchRequest.predicate = predicate
        do {
            if let fetchData = try coreDataStack.persistentContainer.viewContext.fetch(fetchRequest) as? [User], fetchData.count == 1 {
                if let fetchedProfileImage = fetchData[0].profileImage {
                    newUserData.profilePhoto = UIImage(data: fetchedProfileImage)
                } else { newUserData.profilePhoto = nil }
                newUserData.name = fetchData[0].name
            }
        } catch {
        }
        assignUserData()
    }
    
    @IBAction func editProfile(_ sender: Any) {
        let editVC = storyboard?.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        editVC.modalPresentationStyle = .fullScreen
        editVC.newUserData = newUserData
        editVC.homeScreenDeletgate = self
        present(editVC, animated: true, completion: nil)
    }
    @objc func tapGestureAction(tapGesture: UITapGestureRecognizer) {
        let detailImageVC = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        if let profilePhoto = newUserData.profilePhoto {
            detailImageVC.imageFile = profilePhoto
        } else {
            let alertController = UIAlertController(title: "No Image !", message: "no image is available for this profile", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {action in
                
            }))
            present(alertController, animated: true, completion: nil)
        }
        navigationController?.pushViewController(detailImageVC, animated: true)
    }
    private func setUpTapGesture() {
        let tapGesture  = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction(tapGesture:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(tapGesture)
    }
}
extension HomeScreenController: HomeScreenDelegate {
    func passProfileImage(imageToPass: UIImage?) {
        if let fetchedImage = imageToPass {
            newUserData.profilePhoto = fetchedImage
        } else { newUserData.profilePhoto = nil }
    }
}
