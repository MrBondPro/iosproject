//
//  EditViewController.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import CoreData
class EditViewController: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var deleteButton: CustomButton!
    @IBOutlet weak var editImageView: UIImageView!
    var coreDataStack = CoreDataStack()
    var userData: User?
    var newUserData = UserProfile()
    var fetchedImage: UIImage?
    weak var homeScreenDeletgate: HomeScreenDelegate!
    //MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
        assignUserData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if coreDataStack.persistentContainer.viewContext.hasChanges {
            do {
                try coreDataStack.persistentContainer.viewContext.save()
            } catch {
            }
        }
    }
    //MARK: - Handlers
    private func assignUserData() {
        if let profilePhoto = newUserData.profilePhoto {
            editImageView.image = profilePhoto
            deleteButton.isHidden = false
        }
        else {
            editImageView.image = UIImage(named: "default_Profile_Photo")
            deleteButton.isHidden = true
        }
    }
    @IBAction func addNewPhoto(_ sender: Any) {
        let controller = UIAlertController(title: "ADD PROFILE PHOTO", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Gallery", style: .default) { (alertAction) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        let action2 = UIAlertAction(title: "Camera", style: .default, handler: nil)
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action1)
        controller.addAction(action2)
        controller.addAction(CancelAction)
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func deleteExistingPhoto(_ sender: Any) {
        guard editImageView.image != UIImage(named: "default_Profile_Photo") else {
            let alertController = UIAlertController(title: "Nothing to delete", message: "No stored Image available to delete", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {action in
                
            }))
            present(alertController, animated: true, completion: nil)
            return
        }
        editImageView.image = UIImage(named: "default_Profile_Photo")
        homeScreenDeletgate.passProfileImage(imageToPass: nil)
        
        if let fetchedUserProfile = evaluateFetchRequset() {
            fetchedUserProfile.profileImage = nil
        }
        if coreDataStack.persistentContainer.viewContext.hasChanges {
            do {
                try coreDataStack.persistentContainer.viewContext.save()
                let alertController = UIAlertController(title: "Deletion SuccessFul", message: "Image deleted successfuly", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {[unowned self] action in
                    self.dismiss(animated: true, completion: nil)
                }))
                present(alertController, animated: true, completion: nil)
            } catch {
                let alertController = UIAlertController(title: "Error !!", message: "data saving error", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel , handler: {[unowned self] action in
                    self.dismiss(animated: true, completion: nil)
                }))
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    private func evaluateFetchRequset() -> User? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let predicate = NSPredicate(format: "name == '\((newUserData.name)!)'")
        fetchRequest.predicate = predicate
        do {
            if let fetchedData = try coreDataStack.persistentContainer.viewContext.fetch(fetchRequest) as? [User] {
                if fetchedData.count == 1 {
                    return fetchedData[0]
                }
            }
        } catch {
            
        }
        return nil
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        guard let extractedImage = fetchedImage else {
            let alertController = UIAlertController(title: "No Image Found", message: "retry to add image", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                
            }))
            present(alertController, animated: true, completion: nil)
            return
        }
        if let fetchedUserProfile = evaluateFetchRequset() {
            fetchedUserProfile.profileImage = extractedImage.pngData()
        }
        if coreDataStack.persistentContainer.viewContext.hasChanges {
            do {
                try coreDataStack.persistentContainer.viewContext.save()
                let alertController = UIAlertController(title: "Save Successful", message: "new data saved successfully", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {[unowned self] action in
                    self.homeScreenDeletgate.passProfileImage(imageToPass: extractedImage)
                    self.dismiss(animated: true, completion: nil)
                }))
                present(alertController, animated: true, completion: nil)
            } catch {
                let alertController = UIAlertController(title: "Error !!", message: "data saving error", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel , handler: {[unowned self] action in
                    self.dismiss(animated: true, completion: nil)
                }))
                present(alertController, animated: true, completion: nil)
            }
            }
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSignout(_ sender: Any) {
        UserDefaults.standard.set("SignedOut", forKey: "rootView")
        if let fetchedUserContext = evaluateFetchRequset() {
            coreDataStack.persistentContainer.viewContext.delete(fetchedUserContext)
            do {
                try coreDataStack.persistentContainer.viewContext.save()
            } catch { }
        }
        let alertController = UIAlertController(title: "Deletion Successful", message: "Profile deleted successfully", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel , handler: {[weak self] action in
            if let this = self {
                let signupVc = this.storyboard?.instantiateViewController(withIdentifier: "SignUpController") as! SignUpController
                signupVc.modalPresentationStyle = .fullScreen
                this.present(signupVc, animated: true, completion: nil)
            }
            
        }))
        present(alertController, animated: true, completion: nil)
        
    }
}
//MARK: - ImagePickerDelegate
extension EditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                editImageView.image = selectedImage
                fetchedImage = selectedImage
            }
            dismiss(animated: true, completion: nil)
        }
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }

}
