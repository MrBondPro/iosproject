//
//  DetailViewController.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import UIKit
class DetailViewController: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var detailImageView: UIImageView!
    var imageFile: UIImage!
    //MARK: - init
    override func viewDidLoad() {
        navigationItem.title = "Profile Photo"
        super.viewDidLoad()
        detailImageView.image = imageFile
    }
    
    //MARK: - Handlers
    
    
}
