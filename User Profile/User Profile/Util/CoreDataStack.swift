//
//  CoreDataStack.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import CoreData
class CoreDataStack {
   private let moduleName = "User_Profile"
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: moduleName)
        container.loadPersistentStores { (storeDescription, error) in
            if error != nil {
                fatalError("error is : \((error?.localizedDescription)!)")
            }
        }
        return container
    }()
    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("data saving error !")
            }
        }
    }
}
