//
//  CustomImage.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class CustomImage: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        cosumizeImageView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        cosumizeImageView()
    }
    private func cosumizeImageView() {
        layer.masksToBounds = true
        layer.cornerRadius = layer.bounds.width / 2
    }
}
