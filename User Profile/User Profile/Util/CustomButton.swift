//
//  CustomButton.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class CustomButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customizeButton()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customizeButton()
    }
    func customizeButton() {
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }
}
