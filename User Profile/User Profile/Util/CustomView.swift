//
//  CustomView.swift
//  User Profile
//
//  Created by Jovial on 1/15/20.
//  Copyright © 2020 Debashish. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class CustomView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customizeView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customizeView()
    }
    func customizeView() {
        layer.masksToBounds = true
        layer.cornerRadius = layer.bounds.width / 2
    }
}
