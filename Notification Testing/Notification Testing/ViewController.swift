//
//  ViewController.swift
//  Notification Testing
//
//  Created by Home on 7/25/19.
//  Copyright © 2019 BsB. All rights reserved.
//

import UIKit
import UserNotifications
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UNUserNotificationCenter.current().getNotificationSettings { settings in
//            
//            switch settings.authorizationStatus {
//            case .notDetermined:
//                print("state : not deletermied")
//            case .authorized, .provisional:
//                print("state : authorized")
//            default:
//                break // Do nothing
//            }
//        }
//        // Do any additional setup after loading the view, typically from a nib.
//        let content = UNMutableNotificationContent()
//        content.title = "Captain Calling !!"
//        content.body = "Call of Duty !"
//        content.sound = UNNotificationSound.default
//        let date = Date().addingTimeInterval(5)
//        let dateComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
//        let triggerNew = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
//        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
//        let request = UNNotificationRequest(identifier: "notificationIdentifier", content: content, trigger: triggerNew)
//        UNUserNotificationCenter.current().add(request) { (error) in
//            // check if any error arries !!!
//        }
//        
//        // second notification :
//        
//        
//        let content1 = UNMutableNotificationContent()
//        content1.title = "Captain Calling : 2 !!"
//        content1.body = "Call of Duty : ghost protocol"
//        content1.sound = UNNotificationSound.default
//        let date1 = Date().addingTimeInterval(10)
//        let dateComponent1 = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1)
//        let triggerNew1 = UNCalendarNotificationTrigger(dateMatching: dateComponent1, repeats: false)
//        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
//        let request1 = UNNotificationRequest(identifier: "notificationIdentifier : 1", content: content1, trigger: triggerNew1)
//        UNUserNotificationCenter.current().add(request1) { (error) in
//            // check if any error arries !!!
//        }
        
        
//        let date1 = Date().addingTimeInterval(5)
//        let dateComponent1 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date1)
        
//        let date2 = Date().addingTimeInterval(10)
//        let dateComponent2 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date2)
        
//        let date3 = Date().addingTimeInterval(15)
//        let dateComponent3 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date3)
        
//        manager.notifications = [
//            Notification(id: "reminder-1", title: "Remember the milk!", datetime: dateComponent1),
//            Notification(id: "reminder-2", title: "Ask Bob from accounting", datetime: dateComponent2),
//            Notification(id: "reminder-3", title: "Send postcard to mom", datetime: dateComponent3)
//        ]
        //manager.schedule()
    }
    let manager = LocalNotificationManager()
    @IBAction func reminder1(_ sender: UIButton) {
        let date1 = Date().addingTimeInterval(5)
        let dateComponent1 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date1)
        manager.notifications = []
        manager.notifications.append(Notification(id: "reminder-1", title: "Remember the milk!", datetime: dateComponent1))
        manager.schedule()
    }
    @IBAction func reminder2(_ sender: UIButton) {
        let date2 = Date().addingTimeInterval(5)
        let dateComponent2 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date2)
        manager.notifications = []
        manager.notifications.append(Notification(id: "reminder-2", title: "Ask Bob from accounting", datetime: dateComponent2))
        manager.schedule()
    }
    @IBAction func reminder3(_ sender: UIButton) {
        let date3 = Date().addingTimeInterval(5)
        let dateComponent3 = Calendar.current.dateComponents([.year, .month, .hour, .minute, .second], from: date3)
        manager.notifications = []
        manager.notifications.append(Notification(id: "reminder-3", title: "Send postcard to mom", datetime: dateComponent3))
        manager.schedule()
    }
}
