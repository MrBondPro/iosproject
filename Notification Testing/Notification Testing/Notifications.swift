//
//  Notifications.swift
//  Notification Testing
//
//  Created by Home on 7/26/19.
//  Copyright © 2019 BsB. All rights reserved.
//

import Foundation
struct Notification {
    var id:String
    var title:String
    var datetime:DateComponents
}
