//
//  LocalNotificationManager.swift
//  Notification Testing
//
//  Created by Home on 7/26/19.
//  Copyright © 2019 BsB. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit
class LocalNotificationManager {
    var notifications = [Notification]()
    
    func schedule()
    {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            
            switch settings.authorizationStatus {
            case .notDetermined:
                print("state : notDetermined")
                self.requestAuthorization()
            case .authorized, .provisional:
                print("state : authorized")
                self.scheduleNotifications()
            case .denied:
                print("request denied , go to setting to allow manually !!")
            default:
                break // Do nothing
            }
        }
    }
    private func requestAuthorization()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            
            if granted == true && error == nil {
                self.scheduleNotifications()
            }
        }
    }
    
    private func scheduleNotifications()
    {
        for notification in notifications
        {
            let content = UNMutableNotificationContent()
            content.title = notification.title
            content.sound = .default
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: notification.datetime, repeats: false)
            let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { error in

                guard error == nil else { return }

                print("Notification scheduled! --- ID = \(notification.id)")
            }
        }
    }
    
    
}
