//
//  ProductDetailTableViewController.swift
//  AppleStore
//
//  Created by Jovial on 03/03/19.
//  Copyright © 2019 Jovial. All rights reserved.
//

import UIKit

class ProductDetailTableViewController: UITableViewController {
    @IBOutlet weak var productDetailImageView: UIImageView!
    @IBOutlet weak var producttDetailLabelView: UITextField!
    @IBOutlet weak var productDetailTextView: UITextView!
   
    var product: Product?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Edit Details"
        
        productDetailImageView?.image = product?.image
        producttDetailLabelView?.text = product?.title
        producttDetailLabelView.delegate = self
        productDetailTextView?.text = product?.description
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let product = product, let textFieldValue = producttDetailLabelView.text {
            product.title = textFieldValue
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 0 && indexPath.row == 0 {
            print("clicked !!! ")
            return indexPath
        } else {
            return nil
        }
    }
}

extension ProductDetailTableViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ProductDetailTableViewController
{
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        productDetailTextView.resignFirstResponder()
        producttDetailLabelView.resignFirstResponder()
    }
}





