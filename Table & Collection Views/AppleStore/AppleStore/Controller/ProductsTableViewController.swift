//
//  ProductsTableViewController.swift
//  AppleStore
//
//  Created by Jovial on 22/02/19.
//  Copyright © 2019 Jovial. All rights reserved.
//

import UIKit

class ProductsTableViewController: UITableViewController {
    
    var productLines: [ProductLine] = ProductLine.getProductLines()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Apple Store"
        //tableView.estimatedRowHeight = tableView.rowHeight
        //tableView.rowHeight = UITableView.automaticDimension
        navigationItem.rightBarButtonItem = editButtonItem
        
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        print("number of section : \(productLines.count)")
        return productLines.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productLines[section].products.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductsTableViewCell
        cell.product = productLines[indexPath.section].products[indexPath.row]
        return cell
     }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let productLine = productLines[section]
        return productLine.name
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("INDEXPATH.SECTION = ::: \(indexPath.section)")
            productLines[indexPath.section].products.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            if productLines[indexPath.section].products.count == 0 {
//                var indexSet = NSMutableIndexSet()
//                indexSet.add(indexPath.section)
//                print("count before : \(productLines.count)")
//                productLines.remove(at: indexPath.section)
//                print("count after : \(productLines.count)")
//                tableView.deleteSections(indexSet as IndexSet, with: UITableView.RowAnimation.automatic)
//                print("count reached 0 !!!!")
                productLines[indexPath.section].name = ""
                
                
            }
        } 
    }
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let productToMove = productLines[sourceIndexPath.section].products[sourceIndexPath.row]
        productLines[destinationIndexPath.section].products.insert(productToMove, at: destinationIndexPath.row)
        productLines[sourceIndexPath.section].products.remove(at: sourceIndexPath.row)
    }
    var selectedProducts: Product?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProducts = productLines[indexPath.section].products[indexPath.row]
        print(productLines[indexPath.section].products[indexPath.row].title)
        performSegue(withIdentifier: "showProductDetail", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "showProductDetail" {
            let productDetailTVC = segue.destination as! ProductDetailTableViewController
            productDetailTVC.product = selectedProducts
        }
    }
}
