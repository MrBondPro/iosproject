	//
//  Product.swift
//  AppleStore
//
//  Created by Jovial on 22/02/19.
//  Copyright © 2019 Jovial. All rights reserved.
//

import Foundation
import UIKit
    
// product class
    
    enum ProductRating {
        case unrated
        case average
        case ok
        case good
        case brilliant
    }
    class Product {
        var image: UIImage
        var title: String
        var description: String
        var rating: ProductRating
        
        init(titled: String, description: String, imageName: String) {
            self.title = titled
            self.description = description
            if let image = UIImage(named: imageName) {
                self.image = image
            } else {
                self.image = UIImage(named: "default")!
            }
            rating = .unrated
        }
    }
    
