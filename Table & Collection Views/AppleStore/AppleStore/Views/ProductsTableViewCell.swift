//
//  ProductsTableViewCell.swift
//  AppleStore
//
//  Created by Jovial on 23/02/19.
//  Copyright © 2019 Jovial. All rights reserved.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    
    var product: Product? {
        didSet{
            self.updateUI()
        }
    }
    func updateUI() {
        productImageView?.image = product?.image
        productTitleLabel?.text = product?.title
        productDescriptionLabel?.text = product?.description
    }
}
